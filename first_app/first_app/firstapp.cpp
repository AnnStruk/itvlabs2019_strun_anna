

#include <iostream>
#include <cmath>
#include <cassert>


using namespace std;

unsigned int sumOfRealPart(float number) {
	unsigned int sum = 0;
	for (int i = 10; i <= 1000; i *= 10) {
		sum += (static_cast<unsigned int>(number*i)) % 10;
	}
	return sum;
}


int main()
{
	assert(sumOfRealPart(1.123f) == 6);
	assert(sumOfRealPart(1.3333333f) == 9);
	assert(sumOfRealPart(-2.22f) == 4);
	assert(sumOfRealPart(1.1f) == 1);
	assert(sumOfRealPart(1.00f) == 0);

	float number;
	cin >> number;
	unsigned int result = sumOfRealPart(abs(number));
	cout << "Result: " << result << endl;
	cin.get();
    return 0;
}

